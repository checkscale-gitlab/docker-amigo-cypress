# docker-amigo-cypress

[![pipeline status](https://gitlab.com/guardianproject-ops/docker-amigo-cypress/badges/master/pipeline.svg)](https://gitlab.com/guardianproject-ops/docker-amigo-cypress/-/commits/master)

Docker image built off of [docker.io/cypress/browsers](https://hub.docker.com/r/cypress/browsers) with dev runtime tools installed.

### Available tags:

The tags are dynamically generated based on the latest built tags [upstream](https://hub.docker.com/r/cypress/browsers/tags).

We currently build the latest browser bundle with the following versions of node:

* 12
* 14

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/guardianproject-ops/docker-amigo-cypress/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).

